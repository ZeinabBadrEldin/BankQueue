/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Standard Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [BANK]
GO
/****** Object:  StoredProcedure [dbo].[Dequeue]    Script Date: 13-Jul-18 11:25:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Dequeue] (
	 @EliteCustomer bit,
	 @TellerID int

	)
AS
SET NOCOUNT ON

SET TRANSACTION ISOLATION LEVEL READ COMMITTED

update Top(1)  ClientQueues  WITH (UPDLOCK, READPAST)
SET StatusID = 2, ServingDate = getdate(),TellerID = @TellerID
OUTPUT inserted.*
From ClientQueues where ID = (
SELECT CASE 
    WHEN EXISTS(
       SELECT top 1 cq.ID
  FROM   ClientQueues cq inner join clients c on c.ID = cq.clientID
  WHERE  @EliteCustomer =1 and c.ClientType =2  and statusID = 1 
  order by cq.ArrivalDate 
    )
    THEN (
         SELECT top 1 cq.ID 
  FROM   ClientQueues cq inner join clients c on c.ID = cq.clientID
  WHERE  @EliteCustomer =1 and c.ClientType =2  and statusID = 1  order by cq.ArrivalDate 
    ) 
	ELSE
	(
	  SELECT top 1 cq.id 
  FROM   ClientQueues cq inner join clients c on c.ID = cq.clientID
  WHERE  statusID = 1  order by cq.ArrivalDate 
	)
   
END
) 

