﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankQueuingSystem.Models
{
    public class BankViewModel
    {

        BankService service = new BankService();
        
        public int ClientsCount
        {
            get { return service.TotalClientsCount(); }
        }

        // bank status flag for the button naming
        public bool CurrentStatus { get {
                if (HttpContext.Current.Application["BankStatus"] != null)
                    return (bool.Parse(HttpContext.Current.Application["BankStatus"].ToString()));
                else
                    return false;
               
            } }
        public string  BankBtnName
        {
            get
            {
                if (CurrentStatus)
                   return "Close";
                else
                   return "Open";
            }
        }
        // the bank status message 
        public string BankStatus
        { get
            {
                bool status=CurrentStatus;
                return string.Format("The Bank is Now {0}", (status) ? "Opened" : "Closed");
            }
        }

        // clients in queue list 
        public IEnumerable<Client> ClientList
        {
            get { return service.GetClientList(); }
        }
        //Tellers list
        public IEnumerable<Teller> TellersList
        {
            get { return service.GetTellers(); }
        }
        // the current status of a given teller
        public string TellerStatus(int TellerID)
        {
            Client currentClient = GetCurrenetServeringClient(TellerID);
            if (currentClient == null)
                return "Idle";
            else
                return string.Format("Client:{0} ({1})",currentClient.ID, (ClientType)currentClient.ClientType);
        }
        //the client that is currently served by a given teller 
        public Client GetCurrenetServeringClient(int TellerID)
        {
            return service.GetCurrenetServeringClient(TellerID);
        }
    }
}