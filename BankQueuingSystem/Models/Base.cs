﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankQueuingSystem.Models
{
    public class Base
    {
        public int ID { get; set; }
        public DateTime CreateDate { get; set; }
    }
}