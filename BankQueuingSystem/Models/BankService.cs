﻿using System;
using System.Collections.Generic;
using System.Data;
using System.EnterpriseServices;
using System.Linq;

namespace BankQueuingSystem.Models
{
    // a class that contains the methods needed for handling the bank simulation
    public class BankService
    {
        /// <summary>
        /// add new Client to the clients queue in DB with random client type.
        /// initially the client is added with status (arrived) also adding his arrival date with the current date.
        /// </summary>
        public void EnQueueClient()
        {
            BaseDAO<Client> clientDAO = new BaseDAO<Client>(null);
            BaseDAO<ClientQueue> queueDAO = new BaseDAO<ClientQueue>(clientDAO.context);
            var newClient = new Client() { ClientType = (byte)new Random().Next(1, 3) };
            var queueEntry = new ClientQueue { Client = newClient, StatusID = (byte)QueueStatus.Arrived, ArrivalDate = DateTime.Now };
            clientDAO.Insert(newClient);
            queueDAO.Insert(queueEntry);
            clientDAO.Save();
        }

        /// <summary>
        /// pull from the client Queue for a given teller
        /// the dequeue method checks if the teller is serving elite cutomers first, it pulls the first elite customer available by the arrival date.
        /// else it pulls the first customer available (whether he is elite customer or ordinary customer) by the arrival date.
        /// </summary>
        /// <param name="Teller"></param>
        /// <returns>Client Queue object</returns>
        public ClientQueue DequeueClient(Teller Teller)
        {
            BaseDAO<ClientQueue> queueDAO = new BaseDAO<ClientQueue>(null);
            var ClientQueue = queueDAO.Dequeue(Teller.ServingElite, Teller.ID);
            return ClientQueue;
        }

        /// <summary>
        /// after the client is served this method updates the End Time of the service and the status of the client to be served.
        /// </summary>
        /// <param name="clientQueue">the client queue object to be updated.</param>
        public void UpdateQueueStatus(ClientQueue clientQueue)
        {
            clientQueue.EndTime = DateTime.Now;
            clientQueue.StatusID = (byte)QueueStatus.Served;
            BaseDAO<ClientQueue> dao = new BaseDAO<ClientQueue>(null);
            dao.Update(clientQueue);
            dao.Save();
        }
        /// <summary>
        /// get the current client that is being served by the given teller.
        /// </summary>
        /// <param name="tellerID"></param>
        /// <returns></returns>
        public Client GetCurrenetServeringClient(int tellerID )
        {
           BaseDAO<ClientQueue> queueDAO = new BaseDAO<ClientQueue>(null);
            var queueEntry = queueDAO.Get(x => x.TellerID == tellerID && x.StatusID == (byte)QueueStatus.Serving).FirstOrDefault();
            if(queueEntry == null)
                return null;
            return queueEntry.Client;

        }

        /// <summary>
        /// get total clients count.
        /// </summary>
        /// <returns></returns>
        public int TotalClientsCount()
        {
            BaseDAO<Client> dao = new BaseDAO<Client>(null);
            return dao.GetAll().Count();
        }

        /// <summary>
        /// get clients that are arrived and have not been served yet.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Client> GetClientList()
        {
            BaseDAO<ClientQueue> dao = new BaseDAO<ClientQueue>(null);
           return  dao.Get(x => x.StatusID == (byte)QueueStatus.Arrived).Select(x => x.Client);
        }

        /// <summary>
        /// get list of tellers 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Teller> GetTellers()
        {
            BaseDAO<Teller> dao = new BaseDAO<Teller>(null);
            return dao.GetAll();
        }

        /// <summary>
        /// get list of all clients with their service history ordered by their arrival dates.
        /// </summary> 
        /// <returns></returns>
        public IEnumerable<ClientQueue> GetQueueHistoryLog()
        {
            BaseDAO<ClientQueue> dao = new BaseDAO<ClientQueue>(null);
            return  dao.GetAll().OrderBy(x => x.ArrivalDate);
        }
    }
}