﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankQueuingSystem.Models
{
    public class ClientQueue:Base
    {
        public int ClientID { get; set; }
        public int? TellerID { get; set; }
        public byte StatusID { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime? ServingDate { get; set; }
        public DateTime? EndTime { get; set; }

        public virtual Teller Teller { get; set; }
        public virtual Client Client { get; set; }

       
    }
}