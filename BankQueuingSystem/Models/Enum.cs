﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankQueuingSystem.Models
{
    public enum ClientType : byte
    {
        Customer = 1,
        ElieteCustomer = 2
    }

    public enum QueueStatus:byte
    {
        Arrived=1,
        Serving=2,
        Served=3
        
    }
        
}