﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BankQueuingSystem.Models
{
    public class BankContext:DbContext
    {
        public BankContext()
        {
            //this.Configuration.ProxyCreationEnabled = false;
        }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Teller> Teller { get; set; }
        public virtual DbSet<ClientQueue> ClientQueue { get; set; }
    }
}