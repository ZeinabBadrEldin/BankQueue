﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;

namespace BankQueuingSystem.Models
{
    public class BaseDAO<T> where T : Base
    {

        public BankContext context;

        // create new context only if the context param is null
        public BaseDAO(BankContext Context)
        {
            if (Context == null)
                context = new BankContext();
            else
                context = Context;
        }

        /// <summary>
        /// Get list of object by condition
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> Get(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required,
               new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted }))
            {
                return context.Set<T>().Where(predicate);
            }
        }

        /// <summary>
        /// Get all objects of type T.
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAll()
        {
            return context.Set<T>();
        }

        /// <summary>
        /// Get specific object by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual T GetById(object id)
        {
            return context.Set<T>().Find(id);
        }

        /// <summary>
        /// Insert new object of T
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Insert(T entity)
        {
            Base obj = (Base)entity;
            obj.CreateDate = DateTime.UtcNow;
            context.Set<T>().Add(entity);
        }

        /// <summary>
        /// Update exist object.
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Update(T entity)
        {
            context.Set<T>().Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }


        /// <summary>
        /// Delete object by ID.
        /// </summary>
        /// <param name="id"></param>
        public virtual void DeleteById(object id)
        {
            T entityToDelete = context.Set<T>().Find(id);
            Delete(entityToDelete);
        }

        /// <summary>
        /// Delete object of T.
        /// </summary>
        /// <param name="entityToDelete"></param>
        public virtual void Delete(T entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                context.Set<T>().Attach(entityToDelete);
            }
            context.Set<T>().Remove(entityToDelete);
        }

        /// <summary>
        /// Save all context changes to DB.
        /// </summary>
        public virtual void Save()
        {
            context.SaveChanges();
        }
        /// <summary>
        /// call stored procedure Dequeue that pull from the clients queue (FIFO) 
        /// if the teller is serving elite cutomers first, it pulls the first elite customer available by the arrival date.
        /// else it pulls the first customer available (whether he is elite customer or ordinary customer) by the arrival date.
        /// </summary>
        /// <param name="eliteCustomer">bool indicates that the current teller serving elite customer first if exists</param>
        /// <param name="tellerID">the teller that is currently dequeue</param>
        /// <param name="contxt">db context</param>
        /// <returns></returns>

        public virtual T Dequeue(bool eliteCustomer,int tellerID, DbContext contxt = null)
        {
            if (contxt == null) contxt = context;
            var parEliteCustomer = new SqlParameter("@EliteCustomer", eliteCustomer);
            var parTellerID= new SqlParameter("@TellerID", tellerID);
            var query = contxt.Database.SqlQuery<T>("EXEC [Dequeue] @EliteCustomer,@TellerID", parEliteCustomer,parTellerID);
            return query.FirstOrDefault();
        }

    }
}