namespace BankQueuingSystem.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BankQueuingSystem.Models.BankContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BankQueuingSystem.Models.BankContext context)
        {
            //seed the DataBase With Tellers 
            for (int i = 1; i <= 10; i++)
            {
                bool ServingElite = (i == 1 || i == 2) ? true : false;
                context.Teller.AddOrUpdate(new Models.Teller() { ID = i, ServingElite = ServingElite ,CreateDate = DateTime.Now});
            }
        }
    }
}
