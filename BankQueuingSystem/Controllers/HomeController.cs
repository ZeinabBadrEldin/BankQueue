﻿using BankQueuingSystem.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace BankQueuingSystem.Controllers
{
    public class HomeController : Controller
    {
        BankService service = new BankService();

        public ActionResult Index()
        {
            BankViewModel bankModel = new BankViewModel();
            return View("Index",bankModel);
        }
        /// <summary>
        /// run a simulation that a teller is serving clients from the clients queue
        /// while the bank is open or there is clients available (because even if the bank is closed we need the tellers still working to serve the available clients till all the clients are being served)
        /// dequeue a client and wait for a random time between 1 sec to 30 sec (simulating the service process).
        /// update the client status to served and log the end time of the service.
        /// </summary>
        /// <param name="teller"></param>
        public void TellerWork(Teller teller )
        {
            var bankStatus = HttpContext.Application["BankStatus"];
            while (bool.Parse(HttpContext.Application["BankStatus"].ToString()) || service.GetClientList().Any())
            {
                ClientQueue client  = service.DequeueClient(teller);
                if(client!=null)
                {
                    int processTime = new Random().Next(1, 31);                    
                    Thread.Sleep(processTime * 1000);
                    service.UpdateQueueStatus(client);
                }

            }
        }
        /// <summary>
        /// while the bank is open or the clients Limits is reached ( so that you can stop the simulation manually before the limit is reached)
        /// enqueue new client every random amount of time.
        /// if clients limit is reached then update the bank status application variable 
        /// </summary>
        public void SimulateClientQueue()
        {
            int clientsCountLimit = int.Parse(ConfigurationManager.AppSettings["ClientsCountLimit"]);
            
            while (bool.Parse(HttpContext.Application["BankStatus"].ToString())&& service.TotalClientsCount() < clientsCountLimit)
            {
                int processTime = new Random().Next(1, 30);
                Thread.Sleep(processTime * 100);
                service.EnQueueClient();                
            }
            if (service.TotalClientsCount() >= clientsCountLimit)
                HttpContext.Application["BankStatus"] = false;
        }

        /// <summary>
        /// refreshing the view every amount of time to moitor the process 
        /// </summary>
        /// <returns></returns>
        public ActionResult RefreshQueue()
        {
            BankViewModel bankModel = new BankViewModel();
            return PartialView("_BankQueue", bankModel);
        }
        
        /// <summary>
        /// gettign the clients history log 
        /// </summary>
        /// <returns></returns>
        public ActionResult HistoryLog()
        {
            IEnumerable<ClientQueue> logs =  service.GetQueueHistoryLog();
            return View(logs);
        }

        /// <summary>
        /// Change the Bank  status
        /// 1- toggle the app variable that holds the bank status
        /// 2-in case of bank open
        /// 2.1 load the tellers by starting thread for each teller that simulate that a teller is serving clients  
        /// 2.2 simulate that clients is arriving every random amount of time 
        /// </summary>
        /// <returns></returns>
        public ActionResult ChangeBankStatus()
        {
            if (HttpContext.Application["BankStatus"] == null)// check if the application variable has not been created yet.
                HttpContext.Application["BankStatus"] = true; // create the application variable with status open.
            else // if it's already exists toggle the value
                HttpContext.Application["BankStatus"] = !bool.Parse(HttpContext.Application["BankStatus"].ToString()); 
            // in case the bank is openning 
            if (bool.Parse(HttpContext.Application["BankStatus"].ToString()))
            {
                ////Load Tellers 
               IEnumerable <Teller> tellers = service.GetTellers();
               foreach(Teller teller in tellers)
                {
                    new Thread(() =>
                    {
                        Thread.CurrentThread.IsBackground = true;
                        TellerWork(teller);

                    }).Start();
                }
                // Simulate Client Queue
                new Thread(() => { Thread.CurrentThread.IsBackground = true; SimulateClientQueue(); }).Start();
            }
           
            return View("_BankQueue", new BankViewModel());
        }

    }
}